from django.views.generic import View
from django.http import JsonResponse
from .models import Scraper
from datetime import datetime
from django.core.serializers.python import Serializer
from threading import Timer

import requests
import re
import json

class JsonSerializer(Serializer):
    def end_object( self, obj ):
        self._current['id'] = obj._get_pk_val()
        self.objects.append( self._current )

def urlSource():
    url = 'https://coinmarketcap.com/'
    return url

# Obtiene el precio actual de una moneda
def getActualPrice(coin):
    html = requests.get(urlSource()).text

    text = 'class="[A-Za-z0-9- ]+">'+coin+'<\/p><div class="[A-Za-z0-9- ]+"><div class="[A-Za-z0-9- ]+">[0-9]+<\/div><p color="text3" class="[A-Za-z0-9- ]+" font-size="1">[A-Za-z0-9- ]+<\/p><\/div><\/div><\/div><\/a><div class="[A-Za-z0-9- ]+"><div><div class="[A-Za-z0-9- ]+"><button style="white-space:nowrap" class="[A-Za-z0-9- ]+">Buy<\/button><\/div><\/div><\/div><\/div><\/td><td><div class="price___3rj7O "><a href="\/currencies\/[A-Za-z0-9- ]+\/markets\/" class="cmc-link">\$([0-9.,]+)|'
    text += '<span>'+coin+'<\/span><span class="crypto-symbol">[A-Z-a-z0-9]+<\/span><\/a><\/td><td><span>\$<!-- -->([0-9.,]+)'
    search = re.search(text, html)

    if(search):
        if(search.group(1)):
            price = search.group(1)
        elif(search.group(2)):
            price = search.group(2)
        value = float(price.replace(',',''))
    else:
        value = -1
    return value

# Actualiza el precio de una moneda en la bd
def updatePrice(coin):
    price = getActualPrice(coin)
    now = datetime.now()
    objects_count = Scraper.objects.filter(currency = coin).count()
    if(objects_count > 0 and price > 0):
        scraper = Scraper.objects.get(currency=coin)
        scraper.value = price
        scraper.value_updated_at = now
        scraper.save()

# Inicia proceso periódico 
def runJob(coin):
    objects_count = Scraper.objects.filter(currency = coin).count()
    if(objects_count > 0):
        scraper = Scraper.objects.get(currency = coin)
        updatePrice(coin)
        Timer(scraper.frequency, runJob,(coin,)).start() 

class ScraperAPI(View):

    def get(self, *args, **kwargs):

        scrapers_obj = Scraper.objects.all()
        serializer = JsonSerializer()
        data = serializer.serialize(scrapers_obj)

        return JsonResponse(status=200, data={"scrapers" : data }, safe=False)
        
    def post(self, request , *args, **kwargs):
        request = json.loads(request.body)
        coin = request['currency']
        frequency = request['frequency']

        if(not coin or not frequency):
            return JsonResponse(status=400, data={'message': "Parámetros incorrectos"})

        now = datetime.now()
        scraper_obj = Scraper.objects.filter(currency = coin)
        if(scraper_obj.count() > 0):
            return JsonResponse(status=400, data={'error': "Ya existe la moneda buscada"})
        else:
            price = getActualPrice(coin)
            if(price > 0):
                scraper = Scraper(currency=coin, frequency=frequency, value= price, created_at= now, value_updated_at = now)
                scraper.save()
                runJob(coin)
                serializer = JsonSerializer()
                data = serializer.serialize(scraper_obj)
                return JsonResponse(status=200, data=data, safe=False)
            else:
                return JsonResponse(status=404, data={'error': "No se ha encontrado la moneda"})

    def put(self, request,*args, **kwargs):
        
        request = json.loads(request.body)
        id_scraper = request['id']
        frequency = request['frequency']

        if(not id_scraper or not frequency):
            return JsonResponse(status=400, data={'error': "Parámetros incorrectos"})
        objects_count = Scraper.objects.filter(id = id_scraper).count()
        if(objects_count > 0):
            scraper = Scraper.objects.get(id=id_scraper)
            scraper.frequency = frequency
            scraper.save()
            return JsonResponse(status=200, data={'message': "Se ha actualizado la moneda"})
        else:
            return JsonResponse(status=404, data={'error': "No se ha encontrado la moneda buscada"})

    def delete(self, request, *args, **kwargs):

        request = json.loads(request.body)
        id_scraper = request['id']
        if(not id_scraper):
            return JsonResponse(status=400, data={'message': "Parámetros incorrectos"})
        scrapers_count = Scraper.objects.filter(id = id_scraper).count()
        if(scrapers_count > 0):
            Scraper.objects.get(id=id_scraper).delete()
            return JsonResponse(status=200, data={'message': "Se ha eliminado la moneda"})
        else:
            return JsonResponse(status=404, data={'error': "No se ha encontrado la moneda buscada"})

