# Generated by Django 3.0.7 on 2021-03-25 19:35

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='scraper',
            options={'ordering': ['currency']},
        ),
        migrations.AddField(
            model_name='scraper',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='scraper',
            name='currency',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AddField(
            model_name='scraper',
            name='frequency',
            field=models.IntegerField(null=True),
        ),
    ]
