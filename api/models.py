from django.db import models
from django.utils import timezone

class Scraper(models.Model):
    
    created_at = models.DateTimeField(default=timezone.now)
    currency = models.CharField(max_length=64, null=True)
    frequency = models.IntegerField(null=True)
    value = models.FloatField(null=True)
    value_updated_at = models.DateTimeField(null=True)

    def __str__(self):
        return self.currency

